import request from 'supertest'
import { DataSource } from 'typeorm'
import { App } from '../server'
import AuthRepository from '../services/auth.service'

const dataSource = new DataSource({
  type: 'postgres',
  host: 'localhost',
  port: 5432,
  username: 'postgres',
  password: 'rootroot',
  database: 'typeorm',
  synchronize: true,
  logging: false,
  entities: ['./src/entity/*.ts'],
  migrations: [],
  subscribers: []
})

describe('Server', () => {
const server = new App()

  beforeAll(async () => {
    await dataSource.initialize()
     await server.start()
  })

  afterAll(async () => {
   await dataSource.destroy()
   await server.stop()
  })

  it(' should return Hello World! on GET /', async () => {
    const response = await request(server.app).get('/')
    expect(response.status).toBe(200)
    expect(response.text).toBe('Hello World!')
  })

  it('should return a list of news posts', async () => {
      const response = await request(server.app).get('/api/newsposts')
      expect(response.statusCode).toBe(200)
      expect(response.body).toBeInstanceOf(Object)
      expect(response.body.data.items).toBeInstanceOf(Array)
      expect(response.body.data.items.length).toBeGreaterThan(0)
      expect(response.body.data.maxPagesCount).toBeGreaterThan(0)
      expect(response.body.data.items[0]).toHaveProperty('id')
      expect(response.body.data.items[1]).toHaveProperty('title')
      expect(response.body.data.items[2]).toHaveProperty('text')
      expect(response.body.data.items[3]).toHaveProperty('author')
    })

  it('should return 401 for unauthenticated access', async () => {
    await request(server.app)
    .post('/api/newsposts')
    .expect(401)
  })

  it('should return 200 for authenticated access', async () => {
    const res = await request(server.app)
      .get('/api/newsposts')
      .set('Authorization', 'Bearer yourAuthTokenHere')
    expect(res.status).toBe(200)
  })

  it('should generate a valid token', async () => {
    const email = 'test@example.com'
    const password = 'testPassword'

    const token = await AuthRepository.generateToken(email, password)

    expect(token).toBeDefined()
    expect(typeof token).toBe('string')
  })

   it('should return 400 if passwords do not match', async () => {
     const response = await request(server.app)
       .post('/api/auth/register')
       .send({
         email: 'test@example.com',
         password: 'password',
         confirmPassword: 'differentpassword'
       })

     expect(response.status).toBe(400)
     expect(response.body.message).toBe('Passwords do not match')
   })

     it('should return 201 status code and success message on successful registration', async () => {
    const userData = {
      email: 'test@example.com',
      password: 'testpassword',
      confirmPassword: 'testpassword'
    }

    const response = await request(server.app)
      .post('/api/auth/register')
      .send(userData)

    expect(response.status).toBe(201)
    expect(response.body.message).toBe('user created')
  })

    //  it('should create a new post', async () => {
    // const email = 'test@example.com'
    // const password = 'testPassword'
    // const token = await AuthRepository.generateToken(email, password)

    //    const response = await request(server.app)
    //      .post('/api/newsposts')
    //      .set('Authorization', `Bearer ${token}`)
    //      .send({ title: 'Test Post', text: 'This is a test post.', author: 'Test Author' })
    //      expect(response.unauthorized).toBe(false)
    //   //  expect(response.statusCode).toBe(201)
    //    expect(response.body).toHaveProperty('id')
    //    expect(response.body).toHaveProperty('title', 'Test Post')
    //    expect(response.body).toHaveProperty('text', 'This is a test post.')
    //  })
})
